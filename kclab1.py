import shutil
import os
import base64
import math


def open_file_for_reading(file):
    with open(file, 'r', encoding='utf-8') as file:
        data = file.read().replace('\n', '')
    return data


def number_of_symbol(text):
    all_freq = {}
    for i in text:
        if i in all_freq:
            all_freq[i] += 1
        else:
            all_freq[i] = 1
    return all_freq


def entropy(dict):
    entropy = 0
    sum_of_all_symbols = 0
    for symbols in dict:
        sum_of_all_symbols += int(dict[symbols])

    for item in dict:
        entropy -= (dict[item] / sum_of_all_symbols) * math.log2(dict[item] / sum_of_all_symbols)
    return entropy, sum_of_all_symbols


def freq_of_symbol(freq, entropy):
    a = freq
    a = {k: v / entropy[1] for k, v in a.items()}
    return a


def decode_base64(file):
    ct = open_file_for_reading(file)
    string_base64 = ''
    i = 0
    # from the base64 table, convert it to binary
    base64_dict = {"110000": "w", "110001": "x", "110101": "1", "110100": "0", "010100": "U", "010101": "V",
                   "001100": "M", "001101": "N", "011110": "e", "011111": "f", "001001": "J", "001000": "I",
                   "011011": "b", "011010": "a", "000110": "G", "000111": "H", "000011": "D", "000010": "C",
                   "100100": "k", "100101": "l", "111100": "8", "111101": "9", "100010": "i", "100011": "j",
                   "101110": "u", "101111": "v", "111001": "5", "111000": "4", "101011": "r", "101010": "q",
                   "110011": "z", "110010": "y", "010010": "S", "010011": "T", "010111": "X", "010110": "W",
                   "110110": "2", "110111": "3", "011000": "Y", "011001": "Z", "001111": "P", "001110": "O",
                   "011101": "d", "011100": "c", "001010": "K", "001011": "L", "101101": "t", "000000": "A",
                   "000001": "B", "100111": "n", "100110": "m", "000101": "F", "000100": "E", "111111": "/",
                   "111110": "+", "100001": "h", "100000": "g", "010001": "R", "010000": "Q", "101100": "s",
                   "111010": "6", "111011": "7", "101000": "o", "101001": "p"}
    ct_bi8 = ''.join([format(bits, '08b') for bits in ct.encode('utf8')])
    chunks_6bit = [ct_bi8[bits:bits + 6] for bits in range(0, len(ct_bi8), 6)]
    if (len(chunks_6bit[-1]) % 6) < 6:
        new_elem = chunks_6bit[-1]
        if (len(chunks_6bit[-1]) % 6) != 0:
            chunks_6bit[-1] = new_elem + (6 - len(chunks_6bit[-1]) % 6) * '0'
        else:
            pass

    while i < len(chunks_6bit):
        string_base64 += base64_dict[f'{chunks_6bit[i]}']
        i += 1
    c_len_64 = len(string_base64) % 4
    if c_len_64 == 1:
        string_base64 += '==='
    if c_len_64 == 2:
        string_base64 += '=='
    if c_len_64 == 3:
        string_base64 += '='
    with open(f'{file}.base64', 'w') as write_file:
        write_file.write(string_base64)
    return string_base64


def check_size_of_file(file):
    return os.path.getsize(f'{file}')


def all_lab(string):
    file = open_file_for_reading(string)
    print('freq of symbols: ', freq_of_symbol(number_of_symbol(file), entropy(number_of_symbol(file))))
    print('number of 1 symbol in text: ', number_of_symbol(
        file))
    print('entropy: ', entropy(number_of_symbol(file)))
    print('information amount in bytes: ',
          entropy(number_of_symbol(file))[0] * entropy(number_of_symbol(file))[1] * 0.125)


file = open_file_for_reading('ksenos.txt')


def create_archive(file_to_zip):
    # file to zip
    target_path = 'C:\\Users\\oleks\\PycharmProjects\\kslabs\\'  # dir, where file is

    try:
        shutil.make_archive(target_path + f'{file_to_zip}', 'zip', target_path, file_to_zip)
        shutil.make_archive(target_path + f'{file_to_zip}', 'tar', target_path, file_to_zip)
        shutil.make_archive(target_path + f'{file_to_zip}', 'bztar', target_path, file_to_zip)
        shutil.make_archive(target_path + f'{file_to_zip}', 'gztar', target_path, file_to_zip)
    except OSError:
        pass


def get_info_about_all(file):
    print(f'size of {file} in bytes:', check_size_of_file(file))
    print(f'size of {file}.zip in bytes:', check_size_of_file(f"{file}.zip"))
    print(f'size of {file}.tar in bytes:', check_size_of_file(f"{file}.tar"))
    print(f'size of {file}.tar.bz2 in bytes:', check_size_of_file(f"{file}.tar.bz2"))
    print(f'size of {file}.tar.gz in bytes:', check_size_of_file(f"{file}.tar.gz"))


def base_64_check(file):
    text = open_file_for_reading(file)
    message_bytes = text.encode('utf-8')
    base64_bytes = base64.b64encode(message_bytes)
    base64_message = base64_bytes.decode('utf-8')
    return base64_message

def archive_to_base64(file):
    with open(file, mode='rb') as file:  # b is important -> binary
        fileContent = file.read()
    ct = str(fileContent)
    list1 = list(ct)
    list1[0]=''
    ct = ''.join(list1)
    string_base64 = ''
    i = 0
    # from the base64 table, convert it to binary
    base64_dict = {"110000": "w", "110001": "x", "110101": "1", "110100": "0", "010100": "U", "010101": "V",
                   "001100": "M", "001101": "N", "011110": "e", "011111": "f", "001001": "J", "001000": "I",
                   "011011": "b", "011010": "a", "000110": "G", "000111": "H", "000011": "D", "000010": "C",
                   "100100": "k", "100101": "l", "111100": "8", "111101": "9", "100010": "i", "100011": "j",
                   "101110": "u", "101111": "v", "111001": "5", "111000": "4", "101011": "r", "101010": "q",
                   "110011": "z", "110010": "y", "010010": "S", "010011": "T", "010111": "X", "010110": "W",
                   "110110": "2", "110111": "3", "011000": "Y", "011001": "Z", "001111": "P", "001110": "O",
                   "011101": "d", "011100": "c", "001010": "K", "001011": "L", "101101": "t", "000000": "A",
                   "000001": "B", "100111": "n", "100110": "m", "000101": "F", "000100": "E", "111111": "/",
                   "111110": "+", "100001": "h", "100000": "g", "010001": "R", "010000": "Q", "101100": "s",
                   "111010": "6", "111011": "7", "101000": "o", "101001": "p"}
    ct_bi8 = ''.join([format(bits, '08b') for bits in ct.encode('utf8')])
    chunks_6bit = [ct_bi8[bits:bits + 6] for bits in range(0, len(ct_bi8), 6)]
    if (len(chunks_6bit[-1]) % 6) < 6:
        new_elem = chunks_6bit[-1]
        if (len(chunks_6bit[-1]) % 6) != 0:
            chunks_6bit[-1] = new_elem + (6 - len(chunks_6bit[-1]) % 6) * '0'
        else:
            pass

    while i < len(chunks_6bit):
        string_base64 += base64_dict[f'{chunks_6bit[i]}']
        i += 1
    c_len_64 = len(string_base64) % 4
    if c_len_64 == 1:
        string_base64 += '==='
    if c_len_64 == 2:
        string_base64 += '=='
    if c_len_64 == 3:
        string_base64 += '='
    with open(f'text1.base64', 'w') as write_file:
        write_file.write(string_base64)
    return string_base64
# entropy(file)
create_archive('ksenos.txt')
all_lab('ksenos.txt')
print('hand made: ', decode_base64('ksenos.txt'))
print('check: ', base_64_check('ksenos.txt'))
archive_to_base64('ksenos.txt.tar.bz2')
get_info_about_all('ksenos.txt')
print('size file base64:', check_size_of_file('ksenos.txt.base64'))
all_lab('text1.base64')

